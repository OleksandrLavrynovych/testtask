﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class OperetionButton : MonoBehaviour
{
    [SerializeField] private string _operator;

    public static event UnityAction<string> ButtonClicked;

    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(OnButtonClick);
    }

    private void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        ButtonClicked?.Invoke(_operator);
    }
}
