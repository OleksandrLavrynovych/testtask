﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ValueButton : MonoBehaviour
{
    [SerializeField] private string _value;


    public static event UnityAction<string> ButtonClicked;

    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(OnButtonClick);
    }

    private void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        ButtonClicked?.Invoke(_value);
    }
}
