﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour
{
    [SerializeField] private Text _inputTextLine;
    [SerializeField] private Text _fullInputTextLine;

    private float _firstNumber;
    private float _secondNumber;
    private string _operation = "";


    private void OnEnable()
    {
        ValueButton.ButtonClicked += OnValueButtonClicked;
        OperetionButton.ButtonClicked += OnOperationButtonClick;
    }

    private void OnDisable()
    {
        ValueButton.ButtonClicked -= OnValueButtonClicked;
        OperetionButton.ButtonClicked -= OnOperationButtonClick;
    }

    private void OnValueButtonClicked(string value)
    {
        if (_inputTextLine.text == "бесконечность")
        {
            _inputTextLine.text = "0";
        }

        if (_inputTextLine.text == "0")
        {
            _inputTextLine.text = "";
        }

        if (value == ",")
        {
            if (!_inputTextLine.text.Contains(","))
            {
                if (_inputTextLine.text == "0")
                {
                    _inputTextLine.text += "0" + value;
                }
                else
                {
                    _inputTextLine.text += value;
                }
            }
        }
        else
        {
            _inputTextLine.text += value;
        }
    }

    private void OnOperationButtonClick(string value)
    {        
        if (_inputTextLine.text == "бесконечность")
        {
            _inputTextLine.text = "0";
        }

        if(_firstNumber != 0 && _inputTextLine.text != "0")
        {
            OnEqualButtonClick();
        }

        _operation = value;
        _firstNumber = float.Parse(_inputTextLine.text);

        _inputTextLine.text = "0";
        _fullInputTextLine.text = $"{_firstNumber} {_operation} ";
    }

    public void OnEqualButtonClick()
    {
        _secondNumber = float.Parse(_inputTextLine.text);

        switch (_operation)
        {
            case "+":
                _inputTextLine.text = (_firstNumber + _secondNumber).ToString();
                break;
            case "-":
                _inputTextLine.text = (_firstNumber - _secondNumber).ToString();
                break;
            case "*":
                _inputTextLine.text = (_firstNumber * _secondNumber).ToString();
                break;
            case "/":
                if (_secondNumber != 0)
                {
                    _inputTextLine.text = (_firstNumber / _secondNumber).ToString();
                }
                break;
            case "mod":
                if (_secondNumber != 0)
                {
                    _inputTextLine.text = (_firstNumber % _secondNumber).ToString();
                }
                break;
            case "exp":
                _inputTextLine.text = Mathf.Exp(_secondNumber * Mathf.Log(_firstNumber * 4)).ToString();
                break;
            default:
                break;
        }

        _fullInputTextLine.text = $"{_firstNumber} {_operation} {_secondNumber} = {_inputTextLine.text}";
        _firstNumber = 0;
    }

    public void OnSqrButtonClick()
    {
        if (_firstNumber != 0 && _inputTextLine.text != "0")
        {
            OnEqualButtonClick();
        }

        float value = float.Parse(_inputTextLine.text);
        _inputTextLine.text = (value * value).ToString();
    }

    public void OnSqrtButtonClick()
    {
        if (_firstNumber != 0 && _inputTextLine.text != "0")
        {
            OnEqualButtonClick();
        }

        float value = float.Parse(_inputTextLine.text);
        _inputTextLine.text = Mathf.Sqrt(value).ToString();
    }

    public void OnPlusMinusButtonClick()
    {
        if (_inputTextLine.text[0] != '-')
        {
            _inputTextLine.text = "-" + _inputTextLine.text;
        }
        else
        {
            _inputTextLine.text = _inputTextLine.text.Remove(0, 1);
        }
    }
    public void OnCEButtonClick()
    {
        _inputTextLine.text = "0";
        _fullInputTextLine.text = "";
        _firstNumber = 0;
        _secondNumber = 0;
    }

    public void OnBackSpaceButtonClick()
    {
        if (_inputTextLine.text.Length > 0)
        {
            _inputTextLine.text = _inputTextLine.text.Remove(_inputTextLine.text.Length - 1);
        }

        if (_inputTextLine.text == "")
        {
            _inputTextLine.text = "0";
        }
    }
}
